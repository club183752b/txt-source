我的前面站著一隻鹿。
用漢字寫的話，是鹿。
是的，擁有分枝的角，奈良公園裏熟悉的草食動物。
拿著仙貝就成群結隊，不僅是仙貝，連衣服也變得粘糊糊的，
全身都被鹿的口水弄得黏糊糊的，真是個可怕的害獸。
但是，我不是特別討厭那樣圓溜的眼睛的害獸們。
「快看這邊！喂，是鹿！有鹿！」
不由得用歡樂的聲音跟哥雷搭話了。
(哥雷)似乎對鹿沒有那麼感興趣的樣子。
對我的話做出了反應，輕輕地投了視線，不過馬上把頭轉向了。
不是鹿，而是看著我。
{大致上應該是指看了鹿一下後看著男主吧}
第一次遇見猴子的時候，也是這種感覺。
但是，我的情緒卻上升了。
不管怎麼說，說到我在這個世界看到的脊椎動物，那也只有猴子、大叔、恐龍。
突然出現的鹿。
在這些成員中，鹿可以稱得上是異乎尋常的治癒度的動物。
話雖如此，這個世界的鹿似乎比較有威壓感。
外表的印象，與其說是身邊的日本鹿，
不如說更接近馴鹿或黑鹿。兩角都非常巨大。
肩高有2米，這時就已經不是普通的鹿，不過，
(如果)再加上這個巨大的角的話全體的高度達到4米附近。
總之很大。
吊起的那雙眼睛和不祥而扭曲的紅黑色的角，因人而異，可能讓人覺得噁心。
但是，我不會用那種無聊的外表來判斷動物的價值觀。
因為那裡只有雄偉的自然造型美。

「怎麼說呢……真美啊」
無意中我嘟噥了一下，不知什麼時候，這哥雷站在了鹿的面前。
緊接著，她對著鹿，放出了閃電般的高踢。
鹿的頭部受到了可怕的死亡一擊，被炸飛的頭顱噴灑著腦漿。
------


「聽好了哥雷，對其他動物動粗的行為，一般來說是不行的」
我一邊走在街道上，一邊向哥雷解釋著世間的道理。
哥雷長長的耳朵微微顫動著。
被搭話，是高興時的反應。
{話しかけられたりして}
總之，沒有深刻反省樣子。
只是，我無法強烈地注意也是有理由的。
{ただ、俺が強く注意したりできないのにも理由があった。}
也可以看出哥雷對野生動物的神經質。
不管怎麼說，這傢伙從出生到現在，只遇到過敵對的野生動物。
猴子和恐龍都突然來殺我們
(雖然)那些傢伙是魔獸所以(可以)認為是特殊的例子，
不過，我本身，對這個世界的動物生態沒有任何的了解。
既然哥雷排除那隻鹿的判斷可能是正確的，就不負責任的責備他。
話雖如此，終於出現了猴子以外的動物。




我們繼續沿著一條街道向西走。
周圍的情況已經發生了明顯的變化。
據斯伯利亞先生說，這個地區一直刮著著強勁的西風，
如果土屬瘴氣的發源是古代地龍的話，受害者以此為起點集中在東側。
正是如此。從昨天開始，不毛的大地開始脫落，草地和樹木開始到處可見。
最後一次見到猴子也是很久以前的事了。
照這個樣子，也許可以期待位於街道前方的下一個村落。
我重新打開了地圖。
「下一個村落的名字是……“蒂巴拉”嗎？」
來到這裡，也經過著孤零零地被遺棄的村落遺蹟。
與完全沒有村落痕跡的撒瑪利亞以東相比，越往西走，
沿街道的村落間距越短。
這個地方大概以西側為中心，越往東邊境越近吧。
這個國家的首都帝都，在這條街道的遙遠的西邊。
因此，如果一邊確認地圖一邊好好地分配步調，
能容易借住村落的空房子，從那以後完全沒有露宿的必要。
但是，差不多要到達有活人的地方了.
想念軟綿綿的床。雖然手頭的糧食雖然味道也不錯，
但也想嚐嚐這個世界上正常的飲食的味道。
雖說如此，從地圖上的感覺來看，應該差不多接近村落了。
越過那座山坡的那一帶，也許能看見什麼。
「哦，是有人的村莊……！」
從綠色山丘上眺望著街道的前方的我，不由得歡呼起來。
從眼下的房屋中，可以確認似乎有炊事的煙霧一樣的東西在上升。
我的願望似乎是實現了。
蒂巴拉竟然有住著人。
顯而易見，從房子的(數量)規模上來說遠比以前的村落大得多。
氣氛也不像農村，這……已經是街道了。
やったぞ、文明だ！
{太好了? 成功了?求助}
這樣一來，就不必擔心死在野外了。
到現在為止，因為除了神祕的不可思議猴子以外什麼都沒有，所以連自給自足的生存生活都不可能了。
—總算能從那裡脫離的這種安心感，是無法比擬的。
即使決定今後的行動方針，首先也要從那裡開始。
餓死的話，可就本末倒置了。
不久，就到了蒂巴拉城門前。
雖然大門很氣派，但是好像很久沒被利用了。
難道不是幽靈街嗎？從剛才從山丘上看的樣子，街上肯定有人。
回想起來，從這裡往東只住著猴子。沒必要開著門。哪兒有別的門？
暫且，我決定先順時針繞到街的南邊。
走在城外，重新看了看蒂巴拉的街道。
到現在為止的村落遺蹟，到處都是腐朽的木柵欄，是沒有保護性的農村風。
但是，這個城市雖然簡單，但是周圍被土牆包圍著。
雖然也有像看守城樓一樣的東西，但整體上氣氛還是很鬆弛的。
恐怕連我們也沒注意到吧。真悠閒
牆壁也許只是躲避害獸。
嘛，作為這個世界害獸的首領 猴子射出的必殺橄欖球，
這種程度的土牆應該能穿透幾枚。
隔著牆看的街道的外緣，只有屋頂比較新。
剛才從山丘上眺望的房子，看起來已經過了相當久的歲月了。
於是，我突然想到一些逃離了被土屬瘴氣侵襲的地區的人們，
也許已經移居到這個城市了。莫非這就是與印象裡的地圖和之前的村落相比，
規模看起來非常大的理由之一嗎？
街的南側也沒有類似入口的東西。
就這樣我們兩個人，順著外周朝街的東邊走去。
不管怎麼說，這樣下去要繞街半圈了。
哦，南邊有一片田地啊。這是什麼？麥子嗎？
遠處的田地裡可以看到幾個晃動的影子。
是在幹農活的人。
哦，是人……。不是猴子啊，是活生生人……！
{稍微偏離原文，但這樣比較順
おお、人だ……。猿ではないぞ、人があんなに……！}
還有女孩子！
沒什麼大不了的，是幫父母的忙吧，樸素的鄉下姑娘。
也不是說對那個女孩子有什麼特別的興趣。
或者說，太遠了，連臉都不知道。

說不定只是個矮個子的老奶奶。
但是，我感動的顫抖著。
在只有白骨屍體、大叔和一堆猴子的異世界生活裡，終於出現了一個女人。
彷彿是神幫我奪回了滋潤生命的珍貴東西一樣。
{神が取り戻してくれたような気すらした。}
我一邊向神感謝，一邊走在麥田的岔路上，突然停住了腳步。
外牆上有門。街的入口。
已經從東邊繞城半周，來到了西邊。
也就是說，這裡就是蒂巴拉街道的西門。

從西門，(穿過)街道向西，再向外延伸。從這個樣子來看，
這個蒂巴拉原本是街道由東向西穿越正中央的圓形街道的形式吧。
由於土壤瘴氣的侵害，無法向東側推進，
所以關閉了東門，成了一條無法通行的街道。
　門前有幾個出入小鎮的人。

　哇，是人啊……。

　不，雖然遇到每個人都很感動，但這也是沒辦法的事。至今為止，除了斯伯利亞老師以外的靈長類，全都是骨頭和猴子。

　出入的人好像有點多。

　也有駕駛著載貨馬車的商人似的人物。

　這個世界也有馬啊。嘛，不過猴子和鹿也有。

　大家的服裝雖然各有特色，但穿民族風格服裝的人比例大。

　而且，大部分的人都不是日本人的面容。

　不過，其中只有一人，有著微妙的相似東洋系臉龐的中年女性。

　這麼說來，斯伯利亞老師對我的容貌也並不是那麼吃驚的樣子。

　好像是被誤認為是從東方的紛爭地域湧過來的人。

　也就是說，東方有這樣的民族存在嗎。

　門前姑且站著一位拿著槍的看門大叔。

　說實話，也沒有特別強的感覺。覺得只是穿著廉價的皮鎧，留著鬍子的普通大叔。

　稍微觀察了下情況，大叔就只是站著而已。所有人都不經檢查，普通地進出大門。

　沒有身份檢查嗎。好呀，住所不定·職業魔導王也進去了喲。

　那麼，我也只好進去了。

　擦肩而過的時候多少有些緊張，但大叔與其說在看我，還不如說是像舔遍身體一樣的眼神偷偷盯著走在我身後的哥雷。

　這是帶著漂亮的狗散步時經常會發生的現象。多虧如此，並沒有什麼特意要說的。

　幫大忙了，搭檔。

　穿過大門後，蒂巴拉小鎮裡顯得格外熱鬧。

　人們在路上來來往往。

　像是鎮上住民的人、旅行的人。奔跑著的小孩子

　也有像是帶著家畜的農夫，那個是羊嗎？

　噢，在遠處搬運著大件行李的像是灰色素描人偶的那個，難道不是格雷姆嗎？

　總覺得，店鋪的數量也挺多的。

　在前面的像是蔬菜店一樣的店裡，賣著從未見過的蔬菜和水果。

　相反，熟悉的蔬菜和水果也有很多。我想，這些跟原來世界的品種應該不同。

　現在我所站的這條大馬路，按理說應該是作為街道，橫穿這個蒂巴拉小鎮的中央。恐怕這是這一小鎮的主要街道吧。

　我環視了周圍的街道。

　雖然是比較漂亮的街道，但幾乎都是一層樓的木質建築。

　至今為止看到過的村落遺跡，琉貝烏·扎伊雷的隱蔽房屋，也能感覺到，果然從文明程度上來說，確實給人一種近代以前的印象。

　不過，這個世界存在魔術。與原來的世界有什麼不同，現在還不能預測。

　不管怎麼說，既然我所愛的土屬性是渣屬性的話，那我覺得在土木建築方面上會急劇性上升是不可能的事

　說著說著就傷心起來了。這種話題還是止住吧……。

　說到在意的事，從剛才開始，稀稀拉拉地就能看到像我一樣旅行者似的人的身姿。當然大家的裝備都比我要堅固得多。

　但是，由於土壤的瘴氣，現在這裡應該是這條大道的東邊盡頭吧？事實上，從東邊來的我們沒有和任何人擦肩而過。這個小鎮的東門也好像被封鎖了。
　那麼，這些穿著旅行裝的人到底會去哪裡呢。

　由於各種各樣的疑問和好奇心，我像是鄉下人一樣地東張西望，突然有點介意背後的哥雷就回頭看了看。

　從剛才開始就覺莫名其妙的老實。

　不，老實是向來如此的事，但總覺得有點微妙的不同。

　好不容易來到了小鎮上，卻一點都不高興呢，哥雷這傢伙。

　還是說面向街道和人們的視線，格外冷淡呢。

　最初看到猴子和鹿的時候，我覺得也有稍微好一點的反應。

　至少那時，附和看到猴子而興奮的我，會時不時將視線轉向猴子。但是，現在完全沒有看向人們。

　是因為不喜歡人多的地方嗎？

　有點擔心呢……。

　而且，隨著接近小鎮，哥雷身上的氛圍奇妙地嗶哩嗶哩起來。

　剛才在遠遠地望著田裡的女孩子時，氛圍就已經嗶哩嗶哩的了。

　到底是怎麼了，我也很擔心……。（emm無言以對）

　帶著不安回頭的我，那溫柔的深紅瞳孔看向我。

　那樣的目光和往常一樣，特別溫柔。

　但是，不知為何，我感到莫名其妙的心跳加速。

　再次提醒下哥雷吧。無論什麼事，確認都是很重要的。

「……聽好了，哥雷。在這個小鎮的人和格雷姆，決定不能“攻撃”。能夠攻擊的就只有得到我的許可或者對方出手的時候。要好好地遵守約定，乖乖聽話哦，知道了嗎？」

　我對著哥雷，重新確認了在這個社會生活的最低限度的規則。

　上次，由於無能的我做了“不能毆打”的不充分約束，哥雷就踢了對方，然後發生了悲劇。

　全部都是作為主人的我的責任。哥雷沒有錯。但是，絕對不能重複同樣的失敗。

　一動不動、熱切地注視著拚命、認真向她搭話的我，閃耀著那紅寶石般的瞳孔。

　每當我在她身邊說話時，她那又長又美麗的耳朵就會像很癢一樣地微微晃動著。

　嗯~。這傢伙，真的明白了嗎……。

　我有點不安。

　在哥雷壓倒性的暴力面前，殘酷地一個接著一個被單方面蹂躪的聖堂的爆ru格雷姆們的身姿在腦中浮現。

　……不，肯定沒有問題。因為，那個時候的哥雷，不是因為倉皇失措而陷入恐慌狀態了嗎？

　而且原因全是因為我被哥雷誤會的對希臘雕刻的變態行為，深深傷害了純粹無垢的這傢伙的純潔內心。最應該負起責任來切腹的，正是我。

　哥雷非常體貼，非常能理解別人的痛苦，但是自我主張太過謹慎，我也有點但心，不過依然是我引以為豪的搭檔。

　哥雷總是很誠實，很溫柔。

　我從來沒有被這傢伙做出什麼過分的事。

　所以，除了我以外，也應該不會對其他人做很過分的事。

　但是，為什麼呢。為什麼會這麼不安呢？

　我在哥雷面前伸出小指。

「哥雷，像這樣伸出小指來」

　她模仿我的動作，好像有點迷惑的樣子，輕輕地豎起那纖細的小指頭

　我和哥雷拉鉤。

「好啦。……這可是約定哦」

　也許覺得拉鉤很稀奇。

　哥雷輕輕纏著的小指，老是不肯放下來。

　平時這傢伙對我的行為完全沒有抵抗力，到了讓我擔心的程度。總覺得有點少見。